use rouille::Request;
use rouille::Response;
use std::io::Read;

struct PurifiedFile {
    filename: String,
    data: Vec<u8>,
}

fn handle_request(request: &Request) -> Result<PurifiedFile, String> {
  // Filter non-post requests
  if request.method() != "POST" {
    return Err("There are no beasts here".to_owned());
  }

  // Grab request body
  let mut body = match request.data() {
    Some(body) => body,
    None => return Err("Failed to retrieve request body".to_owned()),
  };

  // Read the request body into a byte array
  let mut body_str = String::new();
  if let Err(_) = body.read_to_string(&mut body_str) {
    return Err("Failed to read body to valid UTF-8 string".to_owned());
  }

  // Extract metadata from the acsm file, such as the title and author
  let xml = roxmltree::Document::parse(&body_str)
    .map_err(|_| "Failed to parse ACSM file")?;

  let title = xml
    .descendants()
    .find(|n| n.is_element() && n.has_tag_name("title"))
    .and_then(|n| n.descendants().find(|m| m.is_text()))
    .and_then(|title| title.text())
    .unwrap_or("unknown");

  let author = xml
    .descendants()
    .find(|n| n.is_element() && n.has_tag_name("creator"))
    .and_then(|n| n.descendants().find(|m| m.is_text()))
    .and_then(|author| author.text())
    .unwrap_or("unknown");

  // Write the ACSM data to a file
  if let Err(e) = std::fs::create_dir_all("ebooks") {
    eprintln!("Error creating ebooks directory: {}", e);
    return Err("Internal error 1".to_string());
  }

  let path = format!("ebooks/{title} - {author}");
  let acsm_path = format!("{path}.acsm");

  if let Err(e) = std::fs::write(&acsm_path, body_str.as_bytes()) {
    eprintln!("Error writing acsm: {}", e);
    return Err("Internal error 2".to_string());
  }

  // Run knock, passing the ACSM file file
  let result = std::process::Command::new("./knock")
    .arg(&acsm_path)
    .output();

  match result {
    Ok(out) => if !out.status.success() {
      let stderr = match String::from_utf8(out.stderr) {
        Ok(s) => s,
        Err(_) => {
          eprintln!("Failed to convert knock stderr to utf8");
          return Err("Internal error 3".to_string());
        }
      };

      eprintln!("Error during knock execution: {stderr}");
      return Err(format!("Error downloading EPUB from Adobe Content Server via knock: {stderr}"));
    },

    Err(e) => {
      eprintln!("Error running knock: {}", e);
      return Err("Internal error 4".to_string());
    }
  }

  // Find the file knock output, read it into memory, and return its bytes.
  let epub_path = format!("{path}.epub");
  match std::fs::read(epub_path) {
    Ok(data) => Ok(PurifiedFile {
        filename: format!("{title} - {author}.epub"),
        data,
    }),

    Err(e) => {
      eprintln!("{}", e);
      Err("Internal error 5".to_string())
    }
  }
}

fn main() {
  rouille::start_server("0.0.0.0:8880", move |request| {
    match request.method() {
      "GET" => {
        match std::fs::File::open("index.html") {
          Ok(page) => Response::from_file("text/html", page),

          Err(e) => {
            eprintln!("Failed to open index page: {}", e);
            Response::text("L").with_status_code(500)
          }
        }
      },

      "POST" => match handle_request(request) {
        Ok(purified_file) => {
          Response::from_data("application/epub+zip", purified_file.data)
            .with_additional_header("recommended-name", purified_file.filename)
        },

        Err(e) => Response::text(e).with_status_code(500)
      },

      _ => Response::text("There are no beasts here").with_status_code(404),
    }
  });
}
